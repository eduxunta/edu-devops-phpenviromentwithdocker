#!/bin/bash

OS := $(shell uname)
UID = $(shell id -u)

# From docker-composer-yml
# https://stackoverflow.com/questions/18136918/how-to-get-current-relative-directory-of-your-makefile
CURRENT_FOLDER = $(notdir $(shell pwd))
# https://stackoverflow.com/questions/664601/in-gnu-make-how-do-i-convert-a-variable-to-lower-case
LOWER_CURRENT_FOLDER = $(shell tr '[:upper:]' '[:lower:]' <<< $(CURRENT_FOLDER))
CONTAINER_WEBSERVER_NAME = ${LOWER_CURRENT_FOLDER}_webserver_1
CONTAINER_DB_NAME = mi-db
USER_DB = user
USER_DB_PASS = password
DB_NAME = sample-db

help: ## Show this help message
	@echo '-------------------------------------------------'
	@echo 'Entorno de desarrollo local para PHP ;) '
	@echo '-------------------------------------------------'
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'
	@echo

run-server: ## Start the deployments containers (Detached)
ifeq ($(OS),Darwin)
	U_ID=${UID} docker-compose -f docker-compose.macos.yml up -d
else
	U_ID=${UID} docker-compose -f docker-compose.linux.yml up -d
endif
	@echo '-------------------------------------------------'
	@echo 'Conéctate al web server con http://localhost:8080'
	@echo '-------------------------------------------------'

run-server-atached: ## Start the deployments containers (Atached mode)
ifeq ($(OS),Darwin)
	U_ID=${UID} docker-compose -f docker-compose.macos.yml up
else
	U_ID=${UID} docker-compose -f docker-compose.linux.yml up
endif

stop-server: ## Stop the deployments containers
ifeq ($(OS),Darwin)
	U_ID=${UID} docker-compose -f docker-compose.macos.yml stop
else
	U_ID=${UID} docker-compose -f docker-compose.linux.yml stop
endif

enter-apache: ## ssh's into the be container
	U_ID=${UID} docker exec -it --user root ${CONTAINER_WEBSERVER_NAME} bash

enter-db: ## ssh's into the be container
	U_ID=${UID} docker exec -it --user root ${CONTAINER_DB_NAME} mysql -u${USER_DB} -p${USER_DB_PASS} --database=${DB_NAME}

enter-php-cli: ## Interact with the php interpreter
	U_ID=${UID} docker exec -it --user root ${CONTAINER_WEBSERVER_NAME} php -a

monitoring-webserver: ## Moritoring Web server
	docker logs --follow ${CONTAINER_WEBSERVER_NAME}

monitoring-mysql: ## Moritoring mysql
	docker logs --follow ${CONTAINER_DB_NAME}